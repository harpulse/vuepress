module.exports = {
    extend: '@vuepress/theme-default',
    title: 'GitLab ❤️ VuePress',
    description: 'Vue-powered generator running on GitLab Pages',
    base: '/vuepress/',
    dest: 'public'
}
